var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var twig = require('twig');
var socket_io = require('socket.io');
var passport = require('passport');
var jwt = require('express-jwt');

var app = express();

var io           = socket_io();
app.io           = io;
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(function(req,res,next){

res.header('Access-Control-Allow-Origin',"*");
res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
res.header('Access-Control-Allow-Headers','Content-Type');
next();


})


app.use('/', require('./routes/index'));
app.use('/', require('./routes/facebook'));
app.use('/user', require('./routes/user'));
app.use('/team', require('./routes/team'));
app.use('/posts', require('./routes/posts'));
app.use('/project', require('./routes/project'));
app.use('/communitys', require('./routes/communitys'));
//app.use('/commActivate', require('./routes/commActivate'));
app.use('/mail', require('./routes/mail'));


//app.use('/gaming', require('./routes/gaming'));
//app.use('/mean', require('./routes/mean'));
//app.use('/Photography', require('./routes/Photography'));

app.use('/listpro',require('./routes/form'));

app.use('/analyse_comms', require('./routes/commActivate'));
//app.use('/gaming', require('./routes/gaming'));
//app.use('/mean', require('./routes/mean'));
//app.use('/Photography', require('./routes/Photography'));
/*app.use('/', require('./routes/topics'));*/

app.use('/',require('./routes/twitter'));
/*app.use('/service', require('./routes/service'));*/
app.use('/', require('./routes/form'));

//app.use('/analyse_comms', require('./routes/commActivate'));

//// hazem /////

var allowCrossDomain = function(req, res, next) {
        if ('OPTIONS' == req.method) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
                res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
                res.send(200);
        }
        else {
                next();
        }
};

app.use(allowCrossDomain);

app.use('/chat', require('./routes/chatroom')(io));
app.use('/analyse_comms', require('./routes/commActivate'));

//////////////////
module.exports = app;