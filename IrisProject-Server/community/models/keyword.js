var mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment');
var slug = require('mongoose-slug-generator');

var connection = mongoose.createConnection("mongodb://localhost/mongoose");

autoIncrement.initialize(connection);

mongoose.plugin(slug);


var KeywordSchema = mongoose.Schema({
    Name: String,
    slug: { type: String, slug: "Name", unique: true },
    created: {type: Date, default: Date.now}
});

KeywordSchema.plugin(autoIncrement.plugin, 'Keyword');

module.exports = mongoose.model('Keyword', KeywordSchema);