var mongoose = require('../config/db');

var UserSchema = mongoose.Schema({
    FirstName: String,
    LastName: String,
    Age: Number,
    _community: {type: mongoose.Schema.Types.ObjectId, ref: 'Community'},
    DateInscription: Date,
    Active:Boolean,
    Email:String,
    Image:String,
    Sexe:String,
    Login:String,
    Password:String,
    Interests:[String],
    RewardPoints:Number,
    Discussions:[String]
});

module.exports = mongoose.model('User', UserSchema);