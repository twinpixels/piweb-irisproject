var models = require('../models');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    models.community.find().populate('users').exec(function(err, communitys){
        if(err){
            res.json({error: err});
        }else{
            res.json(communitys);
        }
    });
});

router.get('/:id', function(req, res, next) {
    models.community.findById(req.params.id, function(err, community){
        if(err){
            res.json({error: err});
        }else{
            res.json(communitys);
        }
    });
});

router.put('/:id', function(req, res, next) {
    models.community.findByIdAndUpdate(req.params.id, {$set: {Name: req.body.Name}, $push: {users: req.body.user}}, {new: true}, function(err, community){
        if(err){
            res.json({error: err});
        }else{
            res.json(community);
        }
    });
});

router.post('/', function(req, res, next) {
    var c = new models.community();
    c.Name = req.body.Name;
    c.save(function(err, community){
        if(err){
            res.json({error: err});
        }else{
            res.json(community);
        }
    });
});

router.delete('/:id', function(req, res, next) {
    models.community.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.json({error: err});
        }else{
            res.json({done: 1});
        }
    });
});

module.exports = router;
