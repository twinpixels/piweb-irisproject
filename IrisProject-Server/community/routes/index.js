var express = require('express');
var router = express.Router();
var models = require('../models')

/* GET home page. */
router.get('/', function(req, res, next) {
  var k = new models.keyword();
  k.name = "Keyword 4 De Test";
  k.save(function(err, key){
    if(err){
      res.json({error: err});
    }else{
      res.json(key);
    }
  });
});

/*router.get('/:slug', function(req, res, next) {
  models.keyword.findById(req.params.slug, function(err, k){
    res.json(k);
  });
});*/

module.exports = router;
