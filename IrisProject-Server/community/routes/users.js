var models = require('../models');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    models.user.find().populate('_category').exec(function(err, users){
        if(err){
            res.json({error: err});
        }else{
            res.json(users);
        }
    });
});

router.get('/:id', function(req, res, next) {
    models.user.findById(req.params.id, function(err, p){
        if(err){
            res.json({error: err});
        }else{
            res.json(p);
        }
    });
});

router.put('/:id', function(req, res, next) {
    var data = {
        FirstName: req.body.FirstName,
         LastName: req.body.LastName,
         Age: req.body.Age,
         DateInscription: req.body.DateInscription,
         Active: req.body.Active,
         Interests: req.body.Interests,
         Discussions: req.body.Discussions,
         Email: req.body.Email,
         Image: req.body.Image,
         Sexe: req.body.Sexe,
          Login: req.body.Login,
           Password: req.body.Password,
            RewardPoints: req.body.RewardPoints


 
    
    






    };
    models.user.findByIdAndUpdate(req.params.id, {$set: data}, {new: true}, function(err, p){
        if(err){
            res.json({error: err});
        }else{
            res.json(p);
        }
    });
});

router.post('/', function(req, res, next) {
    var p = new models.user();
    p.FirstName = req.body.FirstName;
    p.LastName = req.body.LastName;
    p._community = req.body.community;
         p.Age= req.body.Age;
        p.DateInscription= req.body.DateInscription;
         p.Active= req.body.Active;
         p.Interests=req.body.Interests;
         p.Discussions= req.body.Discussions;
        p.Email= req.body.Email;
         p.Image= req.body.Image;
         p.Sexe= req.body.Sexe;
          p.Login= req.body.Login;
           p.Password= req.body.Password;
            p.RewardPoints= req.body.RewardPoints;

    p.save(function(err, user){
        if(err){
            res.json({error: err});
        }else{
            res.json(user);
        }
    });
});

router.delete('/:id', function(req, res, next) {
    models.user.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.json({error: err});
        }else{
            res.json({done: 1});
        }
    });
});

module.exports = router;
