var mongoose = require('../config/db.js');

var ProjectSchema = mongoose.Schema({
    Name: String,
    Description: String,
    StartingDate : Date,
    EndingDate : Date,
    MediaContent : String

});

module.exports = mongoose.model('Project', ProjectSchema);