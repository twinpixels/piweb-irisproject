var mongoose = require('../config/db.js');

var PostsSchema = mongoose.Schema({
    Date: Date,
    Content : String,
	Topic : String,
	User : String
	

});

module.exports = mongoose.model('Posts', PostsSchema);