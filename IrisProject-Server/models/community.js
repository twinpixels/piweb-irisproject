var mongoose = require('../config/db');

var communitySchema = mongoose.Schema({
     Name: String,
    Description: String,
    Image:String ,
    DateCreation: Date ,
    tags : Array,
    user: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
});

module.exports = mongoose.model('Community', communitySchema);