var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongoose = require('mongoose');
var UserSchema = require('../routes/schemas/user.js');
var User = mongoose.model('User', UserSchema);
var jwt = require('jsonwebtoken');
var passport = require('../config/passport');
var jwt = require('express-jwt');
var auth = jwt({secret: 'SECRET', userProperty: 'payload'});


//GET : all users
router.get('/getAll', function(req, res, next) {
  User.find(function (err, users) {
    if (err) return next(err);
    res.json(users);
  });
});

//GET : find user by id
router.get('/get/:id', function(req, res, next) {
  User.findById(req.params.id, function (err, usr) {
    if (err) return next(err);
    res.json(usr);
  });
});

//POST : add a new user
router.post('/add', function(req, res, next) {
  User.create(req.body, function (err, usr) {
    if (err) return next(err);
    res.json(usr);
  });
});

//PUT : find and update
router.put('/update/:id', function(req, res, next) {
  User.findByIdAndUpdate(req.params.id, req.body, function (err, usr) {
    if (err) return next(err);
    res.json(usr);
  });
});


/**User.methods.setPassword = function(password){
  this.salt = crypto.randomBytes(16).toString('hex');

  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');
};

User.methods.validPassword = function(password) {
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex');

  return this.hash === hash;
};**/
/**UserSchema.methods.generateJWT = function() {

  // set expiration to 60 days
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    _id: this._id,
    Login: this.Login,
    exp: parseInt(exp.getTime() / 1000),
  }, 'SECRET');
};
router.post('/register', function(req, res, next){
  if(!req.body.Login || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  var user = new User();

  user.Login = req.body.Login;

  user.setPassword(req.body.password)

  user.save(function (err){
    if(err){ return next(err); }

    return res.json({token: user.generateJWT()})
  });
});

router.post('/login', function(req, res, next){
  if(!req.body.Login || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  passport.authenticate('local', function(err, user, info){
    if(err){ return next(err); }

    if(user){
      return res.json({token: user.generateJWT()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
}); **/


//DELETE : find and remove
router.delete('/remove/:id', function(req, res, next) {
  User.findByIdAndRemove(req.params.id, req.body, function (err, usr) {
    if (err) return next(err);
    res.json(usr);
  });
});

module.exports = router;
