module.exports = function(io){

    var express = require('express');
    var router = express.Router();
    //var io = require('socket.io');

    /* GET chatroom page. */
    router.get('/', function(req, res, next) {

        res.render('chatroom.twig', { title: 'Express' });

        io.on('connection', function(socket){
            console.log("a new user has joined the chat!")
            socket.on('send message', function(data){
                io.emit('new message', data);
            });
        });

    });

    return router;

};

