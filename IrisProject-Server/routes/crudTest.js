var express = require('express');
var mongoose = require('mongoose');
var db = require('../routes/mongoDB.js');
var UserSchema = require('../routes/schemas/user.js');

db.once('open', function() {
    console.log("Connected to Database");

    //schemas and models here.

    // USER ----------------------------------

    var User = mongoose.model('User', UserSchema);

    //Display TEST
    var readCB = function (err, data) {
        if (err) return console.error(err);
        else console.log(data);
    }

    User.find(readCB);
});

module.exports = db;
