var express = require('express');
var router = express.Router();
var models = require('../models');
var UserSchema = require('../routes/schemas/user.js');
var User = mongoose.model('User', UserSchema);
var jwt = require('express-jwt');

/* GET home page. */
router.get('/', function(req, res, next) {
    models.team.find({}).exec(function(err, Projects){
        if(err) res.json({error: err});
        res.json(Projects);
    });
});

router.post('/', function(req, res, next) {
    var c = new models.team({Name: req.body.Name});
    c.save(function(err, c){
        if(err) res.json({error: err});
        res.json(c);
    });
});

module.exports = router;