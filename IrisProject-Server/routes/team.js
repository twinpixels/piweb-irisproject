var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var TeamSchema = require('../routes/schemas/team.js');
var Team = mongoose.model('Team', TeamSchema);
var UserSchema = require('../routes/schemas/user.js');
var User = mongoose.model('User', UserSchema);
//var passport = require('../config/passport');
//var jwt = require('express-jwt');
//var auth = jwt({secret: 'SECRET', userProperty: 'payload'});



router.get('/getAll', /*auth,*/ function(req, res, next) {
  Team.find(function (err, team) {
    if (err) return next(err);
      res.setHeader('Access-Control-Allow-Origin', '*');
    res.json(team);
  });
});



router.get('/:id', /*auth,*/ function(req, res, next) {
    Team.findById(req.params.id, function(err, team){
        if(err){
            res.json({error: err});
        }else{
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(team);
        }
        
    });
});

router.post('/add', /*auth,*/ function(req, res, next) {
  Team.create(req.body, function (err, team) {
    if (err) return next(err);
      res.setHeader('Access-Control-Allow-Origin', '*');
    res.json(team);
  });
});



router.put('/:id', /*auth,*/ function(req, res, next) {
    Team.findByIdAndUpdate(req.params.id, req.body, function(err, team){
        if(err){
            res.json({error: err});
        }else{
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(team);
        }
    });
});


router.delete('/:id', /*auth,*/ function(req, res, next) {
    Team.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.json({error: err});
        }else{
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json({done: 1});
        }
    });
});

module.exports = router;