var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var CommunitySchema = require('../routes/schemas/community.js');
var UserSchema = require('../routes/schemas/user.js');
var CommunitySchema = require('../routes/schemas/community.js');
var community = mongoose.model('community', CommunitySchema);

router.get('/', function(req, res, next) {

    res.render('commAnalysis.twig', { title: 'Express' });

    community.once('open', function() {
        console.log("Connected to Database");

        //schemas and models here.

        // USER ----------------------------------

        var Community = mongoose.model('Community', CommunitySchema);
        var User = mongoose.model('User', UserSchema);

        //Display TEST
        var readCB = function (err, data) {
            if (err) return console.error(err);
            else {
                //console.log(data);
                var listComm = data;
                listComm.forEach(function(entry) {
                    var tags = entry.Tags;
                    var interestScore = 0;
                    tags.forEach(function(tag){
                        User.find(function (err, result) {
                            if (err) return console.error(err);
                            var users = result;
                            users.forEach(function(us){
                                var interests = us.Interests;
                                interests.forEach(function(interest){
                                    if (interest == tag){
                                        interestScore++;
                                    }
                                });
                            });
                            if(interestScore>=1){
                                Community.update({ _id: entry._id, State: false }, { State: true }, { multi: true }, function (err, numberAffected, raw) {
                                    if (err) return handleError(err);
                                    console.log("The Community "+ entry.Name +" has been activated !");
                                });
                            }
                        });

                    });
                });
            }
        }

        Community.find(readCB);
    });
});

module.exports = router;