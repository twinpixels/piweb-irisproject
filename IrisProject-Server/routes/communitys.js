var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var CommunitySchema = require('../routes/schemas/community.js');
var community = mongoose.model('community', CommunitySchema);
var UserSchema = require('../routes/schemas/user.js');
var User = mongoose.model('User', UserSchema);
var passport = require('../config/passport');

//var auth = jwt({secret: 'SECRET', userProperty: 'payload'});
//var jwt = require('express-jwt');




router.get('/',  function(req, res, next) {
  community.find(function (err, community) {
    if (err) return next(err);
      res.setHeader('Access-Control-Allow-Origin', '*');
    res.json(community);
  });
});



router.get('/:id',  function(req, res, next) {
    community.findById(req.params.id, function(err, community){
        if(err){
            res.json({error: err});
        }else{
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(community);
        }
        
    });
});

router.post('/add', function(req, res, next) {
  community.create({
    Name : req.body.Name ,
    Description: req.body.Description ,
    Image: req.body.Image
},function(err, community){
    if (err) return next(err);
      res.setHeader('Access-Control-Allow-Origin', '*');
    res.json(community);
  });
});



router.put('/:id',  function(req, res, next) {
    community.findByIdAndUpdate(req.params.id, req.body, function(err, community){
        if(err){
            res.json({error: err});
        }else{
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json(community);
        }
    });
});


router.delete('/:id',  function(req, res, next) {
    community.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.json({error: err});
        }else{
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.json({done: 1});
        }
    });
});

module.exports = router;