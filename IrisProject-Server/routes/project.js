var models = require('../models');
var express = require('express');
var mongoose = require('mongoose');

var UserSchema = require('../routes/schemas/user.js');
var router = express.Router();
var User = mongoose.model('User', UserSchema);
var passport = require('../config/passport');

router.get('/', function(req, res, next) {
    models.project.find(function(err, project){
        if(err){
            res.json({error: err});
        }else{
            res.json(project);
        }
    });
});

router.get('/:id', function(req, res, next) {
    models.project.findById(req.params.id, function(err, project){
        if(err){
            res.json({error: err});
        }else{
            res.json(project);
        }
    });
});

router.put('/:id', function(req, res, next) {
    models.project.findByIdAndUpdate(req.params.id, function(err, project){
        if(err){
            res.json({error: err});
        }else{
            res.json(project);
        }
    });
});

router.post('/', function(req, res, next) {
    var c = new models.project();
    c.Name = req.body.Name,
    c.Description =  req.body.Description,
    c.StartingDate =  req.body.StartingDate,
    c.EndingDate =  req.body.EndingDate;
    c.save(function(err, project){
        if(err){
            res.json({error: err});
        }else{
            res.json(project);
        }
    });
});

router.delete('/:id',  function(req, res, next) {
    models.project.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.json({error: err});
        }else{
            res.json({done: 1});
        }
    });
});

module.exports = router;
