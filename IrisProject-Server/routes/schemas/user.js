var express = require('express');
var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({

       "FirstName": {
            "id": "/FirstName",
            "type": "string"
        },
        "password": {
            "id": "/password",
            "type": "string"
        },
        "LastName": {
            "id": "/LastName",
            "type": "string"
        },
        "Age": {
            "id": "/Age",
            "type": "string"
        },
        "DateInscription": {
            "id": "/DateInscription",
            "type": "object",
            "properties": {}
        },
        "Active": {
            "id": "/Active",
            "type": "boolean"
        },
        "Email": {
            "id": "/Email",
            "type": "string"
        },
        "Image": {
            "id": "/Image",
            "type": "string"
        },
        "Picture":{
            "type":mongoose.Schema.Types.Mixed
        },
        "Sexe": {
            "id": "/Sexe",
            "type": "string"
        },
        "Login": {
            "id": "/Login",
            "type": "string"
        },
        "Interests": {
            "id": "/Interests",
            "type": "array",
        },
        "RewardPoints": {
            "id": "/RewardPoints",
            "type": "number"
        },
        "Teams": {
            "id": "/Teams",
            "type": "array",
        },
        "Discussions": {
            "id": "/Discussions",
            "type": "array",
        },
        "Communities": {
            "id": "/Communities",
            "type": "array",
        },
        "Posts": {
            "id": "/Posts",
            "type": "array",
        },
        "Subsciptions": {
        "id": "/Subsciptions",
        "type": "array"
        },
        "hash": {
        "id": "/hash",
        "type": "string"
        },
        "salt": {
        "id": "/salt",
        "type": "string"
        }


});

UserSchema.pre('save', function(next){
    now = new Date();
    if (!this.DateInscription) {
        this.DateInscription = now;
    }
    next();
});

module.exports = UserSchema;