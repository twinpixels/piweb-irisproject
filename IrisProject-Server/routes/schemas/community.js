var express = require('express');
var mongoose = require('mongoose');

var CommunitySchema = new mongoose.Schema({

    "Name": {
        "id": "/Name",
        "type": "string"
    },
    "Description": {
        "id": "/Description",
        "type": "string"
    },
    "Tags": {
        "id": "/Tags",
        "type": "array"
    },
    "Image": {
        "id": "/Image",
        "type": "string"
    },
    "DateCreation": {
        "id": "/DateCreation",
        "type": "object",
        "properties": {}
    },
    "State": {
        "id": "/State",
        "type": "boolean"
    },
    "Teams": {
        "id": "/Teams",
        "type": "array"
    },
    "Topics": {
        "id": "/Topics",
        "type": "array"
    },
    "Members": {
        "id": "/Members",
        "type": "array"
    },
    "Media": {
        "id": "/Media",
        "type": "array"
    },
    "Medias": {
        "id": "/Medias",
        "type": mongoose.Schema.Types.Mixed
    }


});

module.exports = CommunitySchema;