var express = require('express');
var mongoose = require('mongoose');

var ProjectSchema = new mongoose.Schema({



    "Name": {
        "id": "/Name",
        "type": "string"
    },
    "Description": {
        "id": "/Description",
        "type": "string"
    },
    "StartingDate": {
        "id": "/StartingDate",
        "type": "object",
        "properties": {}
    },
    "EndingDate": {
        "id": "/EndingDate",
        "type": "object",
        "properties": {}
    },
    "MediaContent": {
        "id": "/MediaContent",
        "type": "string"
    },
    "Team": {
        "id": "/Team",
        "type": "string"
    }

});

module.exports = ProjectSchema;