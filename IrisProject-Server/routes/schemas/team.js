var express = require('express');
var mongoose = require('mongoose');

var TeamSchema = new mongoose.Schema({



    "Name": {
        "id": "/Name",
        "type": "string"
    },
    "Image": {
        "id": "/Image",
        "type": "string"
    },
    "Description": {
        "id": "/Description",
        "type": "string"
    },
    "DateCreation": {
        "id": "/DateCreation",
        "type": "object",
        "properties": {}
    },
    "PaypalAccount": {
        "id": "/PaypalAccount",
        "type": "string"
    },
    "Projects": {
        "id": "/Projects",
        "type": "array"
    },
    "Members": {
        "id": "/Members",
        "type": "array"
    },
    "Communities": {
        "id": "/Communities",
        "type": "array"
    },
    "ToDoLIst": {
        "id": "/ToDoLIst",
        "type": "array"
    },
    "Subscribers": {
        "id": "/Subscribers",
        "type": "array"
    }
    


});

module.exports = TeamSchema;