'use strict';

angular.module('myApp.twitter', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/twitter', {
    templateUrl: 'twitter/twitter.html',
    controller: 'twitterCtrl'
  });
}])


.controller('twitterCtrl', function($scope,twitterFactory) {
    
        $scope.twitter=twitterFactory.query();

})


 .factory('twitterFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/twitter',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    })



;