'use strict';

angular.module('myApp.profile', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/profile', {
    templateUrl: 'profile/profile.html',
    controller: 'ProfileCtrl'
  });
}])


    .factory('FindUserByIdFactory', function($resource) {
        return $resource('http://localhost:3000/user/get/:id', { id: '@_id' }, {
            get: {
                method: 'GET' // this method issues a GET request
            }
        });
    })

    .factory('UpdateUserFactory', function($resource) {
        return $resource('http://localhost:3000/user/update/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })

.controller('ProfileCtrl', function($scope,$timeout,FindUserByIdFactory,UpdateUserFactory,filepickerService) {


    $scope.loadUser = function() { //Issues a GET request
        $scope.foundUsr = FindUserByIdFactory.get({"id":"56e2b988c7f5d67328c7e1f1"});
        $scope.changeSuccess = false;
        $scope.changeError = false;
        $scope.pwdDisabled = true;
        $scope.pwdWarning = true;
        $scope.pwdSuccess = false;
    };

    $scope.loadUser();

    //$scope.loadWidget = function(){
    //    var element = document.getElementById('constructed-widget')
    //    element.type="filepicker-dragdrop";
    //    element.setAttribute('data-fp-mimetype', 'image/*');
    //    element.onchange = function(e){
    //        console.log(JSON.stringify(e.fpfile));
    //    };
    //
    //    filepicker.constructWidget(element);
    //}
    //
    //$scope.loadWidget();

    $scope.upload = function(){
        filepickerService.setKey('ACON3kwLySDayuyfTObgmz');
        filepickerService.pick(
            {
                mimetype: 'image/*',
                language: 'en',
                services: ['COMPUTER','DROPBOX','GOOGLE_DRIVE','IMAGE_SEARCH', 'FACEBOOK', 'INSTAGRAM'],
                openTo: 'COMPUTER'
            },
            function(Blob){
                console.log(JSON.stringify(Blob));
                $scope.foundUsr.Picture = Blob;
                $scope.$apply();
            }
        );
    };

    $scope.checkPwd = function() {
        if($scope.pwdDisabled == true && $scope.oldPwd == $scope.foundUsr.Password){
            $scope.pwdDisabled = false;

            $scope.pwdWarning = false;
            $scope.pwdSuccess = true;
        }
    }

    $scope.updateUser = function() { //Issues a PUT
        //console.log($scope.foundUsr);
        var editedUsr = $scope.foundUsr;
        UpdateUserFactory.update({id: editedUsr._id}, editedUsr).$promise.then(function(data) {
            $timeout(function(){
                $scope.changeSuccess = true;
            });
        }, function(error) {
            $timeout(function(){
                $scope.changeError = true;
            });
        });;



    };

})

;