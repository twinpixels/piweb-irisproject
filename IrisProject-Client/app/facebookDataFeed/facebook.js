'use strict';

angular.module('myApp.facebook', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/facebook', {
    templateUrl: 'facebookDataFeed/facebook.html',
    controller: 'facebookCtrl'
  });
}])


.controller('facebookCtrl', function($scope,fbFactory, $http) {
        $http.get('http://localhost:3000/data').success(function(data){
        $scope.data = data ; 
            console.log($scope.data); 
        
        
        }); 

})



.factory('fbFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/data');

    })