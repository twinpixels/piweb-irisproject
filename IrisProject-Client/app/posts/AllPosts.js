'use strict';

angular.module('myApp.AllPosts', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/AllPosts', {
    templateUrl: 'posts/AllPosts.html',
    controller: 'AllPostsCtrl'
  });
}])


.controller('AllPostsCtrl', function($scope,PostsFactory) {
    
        $scope.Posts=PostsFactory.query();

})


 .factory('PostsFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/posts',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    });