'use strict';

angular.module('myApp.login', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'Login/Login.html',
    controller: 'loginCtrl'
  });
}])
