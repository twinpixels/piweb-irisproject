'use strict';

angular.module('myApp.AllCommunitys', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/AllCommunitys', {
    templateUrl: 'community/AllCommunitys.html',
    controller: 'AllCommunitysCtrl'
  });
}])


.controller('AllCommunitysCtrl', function($scope,CommunityFactory) {
    
        $scope.Communitys=CommunityFactory.query();

})


 .factory('CommunityFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/Communitys',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    });