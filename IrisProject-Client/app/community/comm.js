'use strict';

angular.module('myApp.comm', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/comm', {
    templateUrl: 'community/comm.html',
    controller: 'commCtrl'
  });
}])

.factory('CommunityFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/Communitys',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    })

  
  .factory('UpdateCommFactory', function($resource) {
        return $resource('http://localhost:3000/Communitys/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request'http://localhost:3000/team/:id'
            }
        });
    })


    .factory('FindUserByIdFactory', function($resource) {
        return $resource('http://localhost:3000/user/get/:id', { id: '@_id' }, {
            get: {
                method: 'GET' // this method issues a GET request
            }
        });
    })
 .factory('UpdateUserFactory', function($resource) {
        return $resource('http://localhost:3000/user/update/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })





.controller('commCtrl', function($scope,CommunityFactory,UpdateCommFactory,FindUserByIdFactory,UpdateUserFactory) {
    
     
    
         $scope.Communitys=CommunityFactory.query();

    var foundUsr = FindUserByIdFactory.get({"id":"56e2b988c7f5d67328c7e1f1"});

    
    $scope.subscribe = function(chosenComm) { //Issues a PUT

        foundUsr.$promise.then(function(data){
            var subscribed = false;
            var indexForUsr=undefined;
            var indexForComm=undefined;

            for(var i=0;i<data.Subsciptions.length;i++){
                if(data.Subsciptions[i] == chosenComm._id){
                    subscribed = true;
                    indexForUsr = i;

                    for(var j=0;j<chosenComm.Members.length;j++){
                        if(chosenComm.Members[i] == data._id){
                            indexForComm = j;
                        }
                    }
                }
            }



            if(!subscribed){
                data.Subsciptions.unshift(chosenComm._id);
                data.RewardPoints+=100;
                UpdateUserFactory.update({id: data._id}, data);

                chosenComm.Members.unshift(data._id);
                UpdateCommFactory.update({id: chosenComm._id}, chosenComm);
            }
            else{
                data.Subsciptions.splice(indexForUsr,1);
                data.RewardPoints-=100;
                UpdateUserFactory.update({id: data._id}, data);

                chosenComm.Members.splice(indexForComm,1);
                UpdateCommFactory.update({id: chosenComm._id}, chosenComm);
            }

        });

    };

})



 ;