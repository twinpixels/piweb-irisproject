'use strict';

angular.module('myApp.photo', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/photo', {
    templateUrl: 'community/photo.html',
    controller: 'photoCtrl'
  });
}])


.controller('photoCtrl', function($scope,photoFactory) {
    
        $scope.photo=photoFactory.query();

})


 .factory('photoFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/photo',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    })



;