'use strict';

angular.module('myApp.gaming', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/gaming', {
    templateUrl: 'community/gaming.html',
    controller: 'gamingCtrl'
  });
}])


.controller('gamingCtrl', function($scope,gamingFactory) {
    
        $scope.gaming=gamingFactory.query();

})


 .factory('gamingFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/gaming',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    })



;