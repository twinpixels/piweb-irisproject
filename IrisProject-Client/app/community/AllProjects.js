'use strict';

angular.module('myApp.AllProjects', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/AllProjects', {
    templateUrl: 'community/AllProjects.html',
    controller: 'AllProjectsCtrl'
  });
}])


.controller('AllProjectsCtrl', function($scope,ProjectFactory) {
    
        $scope.project=ProjectFactory.query();

})


 .factory('ProjectFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/project',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    })



;