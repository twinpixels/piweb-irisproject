'use strict';

angular.module('myApp.communityHome', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/community/:id', {
    templateUrl: 'community/home.html',
    controller: 'CommunityHomeCtrl'
  });
}])
    .filter('trustUrl', function ($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    })

    .filter("sanitize", ['$sce', function($sce) {
        return function(htmlCode){
            return $sce.trustAsHtml(htmlCode);
        }
    }])

    .factory('FindCommByIdFactory', function($resource) {
        return $resource('http://localhost:3000/communitys/:id', { id: '@_id' }, {
            get: {
                method: 'GET' // this method issues a GET request
            }
        });
    })

    .factory('UpdateUserFactory', function($resource) {
        return $resource('http://localhost:3000/user/update/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })

    .factory('UpdateCommFactory', function($resource) {
        return $resource('http://localhost:3000/communitys/update/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })

    .factory('FindUserByIdFactory', function($resource) {
        return $resource('http://localhost:3000/user/get/:id', { id: '@_id' }, {
            get: {
                method: 'GET' // this method issues a GET request
            }
        });
    })

    .controller('CommunityHomeCtrl', function($scope,$timeout,$routeParams,FindCommByIdFactory,UpdateCommFactory,FindUserByIdFactory,UpdateUserFactory,filepickerService) {


        $scope.loadCommunity = function() { //Issues a GET request
            $scope.foundCommunity = FindCommByIdFactory.get({"id":$routeParams.id});

            var tempComm = FindCommByIdFactory.get({"id":$routeParams.id});

            tempComm.$promise.then(function(data){
                $scope.teamsNbr = data.Teams.length;
                $scope.membersNbr = data.Members.length;

                var mediaList = data.Medias;


                for(var i=0; i<mediaList.length; i++ ){
                    var foundUsr = FindUserByIdFactory.get({"id":mediaList[i].poster});

                    mediaList[i].poster = foundUsr;
                }

                $scope.medias = mediaList;

            });

        };

        $scope.loadCommunity();

        $scope.uploadVid = function(){
            filepickerService.setKey('ACON3kwLySDayuyfTObgmz');
            filepickerService.pick(
                {
                    mimetype: 'video/mp4',
                    language: 'en',
                    services: ['COMPUTER','DROPBOX','GOOGLE_DRIVE','VIDEO'],
                    openTo: 'COMPUTER'
                },
                function(Blob){
                    console.log(JSON.stringify(Blob));
                    var toAdd = {};
                    toAdd.type = "video";
                    toAdd.poster = "56e2b988c7f5d67328c7e1f1";
                    toAdd.content = Blob;
                    toAdd.date = new Date();

                    var Usr = FindUserByIdFactory.get({"id":toAdd.poster});
                    Usr.$promise.then(function(data){
                        data.RewardPoints+=50;
                        UpdateUserFactory.update({id: data._id}, data);
                    });

                    $scope.foundCommunity.Medias.unshift(toAdd);


                    UpdateCommFactory.update({id: $scope.foundCommunity._id}, $scope.foundCommunity).$promise.then(function(data) {
                        $timeout(function(){
                            console.log("add media is a success !");
                            //$scope.loadCommunity();
                        });
                    }, function(error) {
                        $timeout(function(){
                            console.log("add media is a failure !");
                        });
                    });;

                    $scope.medias.unshift(toAdd);
                    $scope.$apply();
                }
            );
        };

        $scope.uploadImg = function(){
            filepickerService.setKey('ACON3kwLySDayuyfTObgmz');
            filepickerService.pick(
                {
                    mimetype: 'image/*',
                    language: 'en',
                    services: ['COMPUTER','DROPBOX','GOOGLE_DRIVE','IMAGE_SEARCH', 'FACEBOOK', 'INSTAGRAM'],
                    openTo: 'COMPUTER'
                },
                function(Blob){
                    console.log(JSON.stringify(Blob));
                    var toAdd = {};
                    toAdd.type = "image";
                    toAdd.poster = "56e2b988c7f5d67328c7e1f1";
                    toAdd.content = Blob;
                    toAdd.date = new Date();

                    var Usr = FindUserByIdFactory.get({"id":toAdd.poster});
                    Usr.$promise.then(function(data){
                        data.RewardPoints+=50;
                        UpdateUserFactory.update({id: data._id}, data);
                    });

                    $scope.foundCommunity.Medias.unshift(toAdd);


                    UpdateCommFactory.update({id: $scope.foundCommunity._id}, $scope.foundCommunity).$promise.then(function(data) {
                        $timeout(function(){
                            console.log("add media is a success !");
                            //$scope.loadCommunity();
                        });
                    }, function(error) {
                        $timeout(function(){
                            console.log("add media is a failure !");
                        });
                    });;

                    $scope.medias.unshift(toAdd);
                    $scope.$apply();
                }
            );
        };

        $scope.shareTxt = function() { //Issues a PUT

            var toAdd = {};
            toAdd.type = "text";
            toAdd.poster = "56e2b988c7f5d67328c7e1f1";
            toAdd.content = {};
            var msg = $scope.msgForShare;
            toAdd.content.message = msg;
            toAdd.date = new Date();

            var Usr = FindUserByIdFactory.get({"id":toAdd.poster});
            Usr.$promise.then(function(data){
                data.RewardPoints+=50;
                UpdateUserFactory.update({id: data._id}, data);
            });

            $scope.foundCommunity.Medias.unshift(toAdd);


            UpdateCommFactory.update({id: $scope.foundCommunity._id}, $scope.foundCommunity).$promise.then(function(data) {
                $timeout(function(){
                    console.log("add media is a success !");
                    //$scope.loadCommunity();
                });
            }, function(error) {
                $timeout(function(){
                    console.log("add media is a failure !");
                });
            });;

            $scope.medias.unshift(toAdd);



        };

    });
