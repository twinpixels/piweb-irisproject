'use strict';

angular.module('myApp.comm', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/comm/:id', {
    templateUrl: 'community/CommunityPage.html',
    controller: 'commCtrl'
  });
}])




.controller('commCtrl', function($scope,CommunityByIdFactory,$routeParams) {
        $scope.comm=CommunityByIdFactory.get({id:$routeParams.id});

})





    .factory('CommunityByIdFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/communitys/:id');

    })

  


;//detail du community dynamique consommé sur angular