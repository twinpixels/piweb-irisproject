angular.module('myApp.services').service('Language', ['$locale', '$http', 'gettextCatalog', '$cookies',
  function($locale, $http, gettextCatalog, $cookies) {
    var supportedLanguages = ['en', 'fr', 'ar']; //Add the supported languages by MySign in this array
    var lang = 'en'; //by default, set the lang cookie in english but we might change it
    
    this.init = function init() {
      if ($cookies.devmode)
        gettextCatalog.debug = true; //display [MISSING] in front of every missing translation
        
      //If the cookie lang is not set, try to fetch the language of the navigator, otherwise, we use english.
      if (!$cookies.lang) {
        var language = window.navigator.language || window.navigator.userLanguage;
        language = language.substr(0,2); // get the lang to the ISO639-1 format 
        console.info('cookie lang not set, language found', language);
        if (supportedLanguages.indexOf(language) !== -1)
          $cookies.lang = language;
        else
          $cookies.lang = lang; 
      }
      gettextCatalog.baseLanguage = lang; //the labels in the app are written in english
      this.set($cookies.lang, 'init');
    };
      
    this.get = function get() {
      return lang;
    };
      
    this.set = function set(language, context) {
      console.info('Set language', language);
      if (supportedLanguages.indexOf(language) === -1)
      {
        console.warn('Language not supported', language);
        return;
      }
      lang = language;
      $locale.id = lang;
      $cookies.lang = lang;
      
      if (typeof context === 'undefined' || context !== 'init') {
        $http({
          method: 'GET',
          url: '/static/languages/'+lang+'.json',
          cache: gettextCatalog.cache,
          transformResponse: function(json) {
            json = json.substr('loaded('.length);
            json = json.substr(0, json.length-1);
            return JSON.parse(json);
          }
        }).success(function (data) {
          for (var lang in data)
            if (data.hasOwnProperty(lang)) {
              gettextCatalog.setStrings(lang, data[lang]);
            }
          gettextCatalog.setCurrentLanguage(lang);
        });
      }
    };  
  }]);