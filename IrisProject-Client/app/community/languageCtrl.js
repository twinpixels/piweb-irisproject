angular.module('myApp').controller('languageCtrl', ['$scope', 'Language', 
  function($scope, Language) {
    $scope.changeLanguage = function changeLanguage(lang) {
      Language.set(lang);
    };  
  }]);
})();