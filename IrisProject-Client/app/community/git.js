'use strict';

angular.module('myApp.git', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/git', {
    templateUrl: 'community/git.html',
    controller: 'gitCtrl'
  });
}])


.controller('gitCtrl', function($scope,gitFactory) {
    
        $scope.git=gitFactory.query();

})


 .factory('gitFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/mean',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    })



;