'use strict';

// Declare app level module which depends on views, and components

angular.module('myApp', [
  'ngRoute',
  'ngResource',
  'gettext',
  'angular-filepicker',
  'myApp.AllProjet',
  'myApp.AllPosts',
  'myApp.SendMail', 
  'myApp.profile',
  'myApp.facebook',
  'myApp.AllTeams',
  'myApp.AddTeam',
  'myApp.team',
  'myApp.communityHome',
  'myApp.AllCommunitys',
  'myApp.twitter',
  'myApp.comm',
  'myApp.AllProjects',
  'myApp.gaming',
  'myApp.photo',
  'myApp.git',
  'myApp.comm',
  'myApp.login',
  'myApp.add',
  'myApp.addp',
  'myApp.version'
    
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/AllCommunitys'});
}]);


