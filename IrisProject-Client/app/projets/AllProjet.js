'use strict';

angular.module('myApp.AllProjet', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/AllProjet', {
    templateUrl: 'projets/AllProjet.html',
    controller: 'AllProjetCtrl'
  });
}])


.controller('AllProjetCtrl', function($scope,ProjetFactory) {
    
        $scope.Projet=ProjetFactory.query();
    

})


 .factory('ProjetFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/listpro',null, 
	{
            'query':  {method:'GET', isArray:true}
        });

    });