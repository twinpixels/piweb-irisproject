'use strict';

angular.module('myApp.AllTeams', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/AllTeams', {
    templateUrl: 'team/AllTeams.html',
    controller: 'AllTeamsCtrl'
  });
}])

    .factory('TeamFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/team/getAll',null,
            {
                'query':  {method:'GET', isArray:true}
            });

    })

    .factory('UpdateUserFactory', function($resource) {
        return $resource('http://localhost:3000/user/update/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })

    .factory('UpdateTeamFactory', function($resource) {
        return $resource('http://localhost:3000/team/:id', { id: '@_id' }, {
            update: {
                method: 'PUT' // this method issues a PUT request
            }
        });
    })

    .factory('FindUserByIdFactory', function($resource) {
        return $resource('http://localhost:3000/user/get/:id', { id: '@_id' }, {
            get: {
                method: 'GET' // this method issues a GET request
            }
        });
    })

.controller('AllTeamsCtrl', function($scope,TeamFactory,UpdateUserFactory,UpdateTeamFactory,FindUserByIdFactory) {
    
        $scope.teams=TeamFactory.query();

    var foundUsr = FindUserByIdFactory.get({"id":"56e2b988c7f5d67328c7e1f1"});

    
    $scope.subscribe = function(chosenTeam) { //Issues a PUT

        foundUsr.$promise.then(function(data){
            var subscribed = false;
            var indexForUsr=undefined;
            var indexForTeam=undefined;

            for(var i=0;i<data.Subsciptions.length;i++){
                if(data.Subsciptions[i] == chosenTeam._id){
                    subscribed = true;
                    indexForUsr = i;

                    for(var j=0;j<chosenTeam.Subscribers.length;j++){
                        if(chosenTeam.Subscribers[i] == data._id){
                            indexForTeam = j;
                        }
                    }
                }
            }



            if(!subscribed){
                data.Subsciptions.unshift(chosenTeam._id);
                data.RewardPoints+=200;
                UpdateUserFactory.update({id: data._id}, data);

                chosenTeam.Subscribers.unshift(data._id);
                UpdateTeamFactory.update({id: chosenTeam._id}, chosenTeam);
            }
            else{
                data.Subsciptions.splice(indexForUsr,1);
                data.RewardPoints-=200;
                UpdateUserFactory.update({id: data._id}, data);

                chosenTeam.Subscribers.splice(indexForTeam,1);
                UpdateTeamFactory.update({id: chosenTeam._id}, chosenTeam);
            }

        });

    };

})






;