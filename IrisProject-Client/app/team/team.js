'use strict';

angular.module('myApp.team', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/team/:id', {
    templateUrl: 'team/team.html',
    controller: 'teamCtrl'
  });
}])


/*.controller('teamCtrl', function($scope,TeamFactory) {
        $scope.teams=TeamFactory.query();

})*/


.controller('teamCtrl', function($scope,TeamByIdFactory,FindUserByIdFactory,$routeParams) {
        $scope.team=TeamByIdFactory.get({id:$routeParams.id});    
    
    
    
        $scope.loadCommunity = function() { //Issues a GET request
            $scope.foundCommunity = TeamByIdFactory.get({"id":$routeParams.id});
            console.log("hello");
            $scope.foundCommunity.$promise.then(function(data){
                var MemberList = data.Members;
                    console.log(MemberList);
                
                console.log("hello");

                for(var i=0; i<MemberList.length; i++ ){
                    var foundUsr = FindUserByIdFactory.get({"id":MemberList[i]});

                    MemberList[i] = foundUsr;
                }

                $scope.Member = MemberList;
                console.log($scope.Member);

            });

        };
    
    $scope.loadCommunity();
    
    
    
})




















     .factory('FindUserByIdFactory', function($resource) {
        return $resource('http://localhost:3000/user/get/:id', { id: '@_id' }, {
            get: {
                method: 'GET' // this method issues a GET request
            }
        });
    })






    .factory('TeamByIdFactory', function($resource){
        //Resource
        return $resource('http://localhost:3000/team/:id');

    })

  /*  .factory('AddTeamtFactory', function($resource){
    
        //Resources
       var managingTeam=$resource('http://localhost:3000/team/add');
          return managingTeam;
    })*/



;