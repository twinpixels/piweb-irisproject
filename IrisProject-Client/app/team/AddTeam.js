'use strict';

angular.module('myApp.AddTeam', ['ngRoute','ngResource'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/AddTeam', {
    templateUrl: 'team/AddTeam.html',
    controller: 'AddTeamCtrl'
  });
}])

.controller('AddTeamCtrl',function($scope, $http){
    
	// post
    $scope.add = function(){
	$http.post('http://localhost:3000/team/add',$scope.team).
        success(function(data) {
            console.log("posted successfully");
        }).error(function(data) {
            console.error("error in posting");
        })
    }
})




;